package co.burcu.rocketsaidhi.base

import android.arch.lifecycle.ViewModel
import co.burcu.rocketsaidhi.injection.component.ViewModelInjector
import co.burcu.rocketsaidhi.injection.module.NetworkModule
import co.burcu.rocketsaidhi.ui.launch.LaunchListViewModel
import co.burcu.rocketsaidhi.injection.component.DaggerViewModelInjector

/**
 * Created by Burcu Yalcinkaya on 05/10/2018.
 */

abstract class BaseViewModel : ViewModel() {
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is LaunchListViewModel -> injector.inject(this)
        }
    }

}