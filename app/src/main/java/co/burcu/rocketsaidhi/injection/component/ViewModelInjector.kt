package co.burcu.rocketsaidhi.injection.component

import co.burcu.rocketsaidhi.injection.module.NetworkModule
import co.burcu.rocketsaidhi.ui.launch.LaunchListViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Burcu Yalcinkaya on 05/10/2018.
 */

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified LaunchListViewModel.
     * @param launchListViewModel LaunchListViewModel in which to inject the dependencies
     */
    fun inject(launchListViewModel: LaunchListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}