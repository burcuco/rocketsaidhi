package co.burcu.rocketsaidhi.injection.module

import co.burcu.rocketsaidhi.BuildConfig.SERVER_URL
import co.burcu.rocketsaidhi.network.RocketApi
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor


/**
 * Created by Burcu Yalcinkaya on 05/10/2018.
 */

@Module

object NetworkModule {

    /**
     * Provides the Launch service implementation.
     * @param retrofit the Retrofit object used to instantiate the service
     * @return the Launch service implementation.
     */

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRocketApi(retrofit: Retrofit): RocketApi {
        return retrofit.create(RocketApi::class.java)
    }

    /**
     * Provides the Retrofit object.
     * @return the Retrofit object
     */
    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRetrofitInterface(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        return Retrofit.Builder()
                .baseUrl(SERVER_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
    }
}