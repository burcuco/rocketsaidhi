package co.burcu.rocketsaidhi.model

import java.io.Serializable

/**
 * Created by Burcu Yalcinkaya on 05/10/2018.
 */

/**
 * Class which provides a model for Launch
 * @constructor Sets all properties of the Launch
 * @property name the name of the Launch
 * @property id the unique identifier of the Launch
 * @property windowstart the start date of the Launch
 * @property windowend the end date of the Launch
 * @property location the Location of the Launch
 * @property rocket the Rocket of the Launch
 */
data class Launch(val id: Int,
                  val name: String,
                  val status: String,
                  val windowstart: String,
                  val windowend: String,
                  val location: Location,
                  val rocket: Rocket) : Serializable