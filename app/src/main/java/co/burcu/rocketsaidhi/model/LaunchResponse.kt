package co.burcu.rocketsaidhi.model

import java.io.Serializable

/**
 * Created by Burcu Yalcinkaya on 05/10/2018.
 */

data class LaunchResponse(val launches: List<Launch>) : Serializable