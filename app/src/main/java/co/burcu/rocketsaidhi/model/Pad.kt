package co.burcu.rocketsaidhi.model

import java.io.Serializable

/**
 * Created by Burcu Yalcinkaya on 06/10/2018.
 */

data class Pad(val id: Int, val name: String, val agencies: List<Agency>) : Serializable