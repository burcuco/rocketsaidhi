package co.burcu.rocketsaidhi.network

import co.burcu.rocketsaidhi.model.LaunchResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Burcu Yalcinkaya on 05/10/2018.
 */
interface RocketApi {

    @GET("/1.3/launch/next/{next}")
    fun getRockets(@Path("next") next: Int): Observable<LaunchResponse>

}