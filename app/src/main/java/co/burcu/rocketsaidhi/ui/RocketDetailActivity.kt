package co.burcu.rocketsaidhi.ui

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import co.burcu.rocketsaidhi.R
import co.burcu.rocketsaidhi.databinding.ActivityRocketDetailBinding
import co.burcu.rocketsaidhi.model.Launch
import co.burcu.rocketsaidhi.ui.rocket.RocketDetailViewModel
import co.burcu.rocketsaidhi.utils.CURRENT_LAUNCH

class RocketDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRocketDetailBinding
    private lateinit var viewModel: RocketDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val launch = intent.extras?.getSerializable(CURRENT_LAUNCH) as Launch

        binding = DataBindingUtil.setContentView(this, R.layout.activity_rocket_detail)
        viewModel = ViewModelProviders.of(this).get(RocketDetailViewModel::class.java)
        binding.viewModel = viewModel
        viewModel.bind(launch)


    }

}
