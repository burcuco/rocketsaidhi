package co.burcu.rocketsaidhi.ui.launch

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import co.burcu.rocketsaidhi.R
import co.burcu.rocketsaidhi.databinding.ItemLaunchBinding
import co.burcu.rocketsaidhi.model.Launch

/**
 * Created by Burcu Yalcinkaya on 05/10/2018.
 */

class LaunchListAdapter : RecyclerView.Adapter<LaunchListAdapter.ViewHolder>() {
    private lateinit var launchList: List<Launch>
    var onItemClick: ((Launch) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LaunchListAdapter.ViewHolder {
        val binding: ItemLaunchBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_launch, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LaunchListAdapter.ViewHolder, position: Int) {
        holder.bind(launchList[position])
    }

    override fun getItemCount(): Int {
        return if (::launchList.isInitialized) launchList.size else 0
    }

    fun updateLaunchList(launchList: List<Launch>) {
        this.launchList = launchList
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemLaunchBinding) : RecyclerView.ViewHolder(binding.root) {
        private val viewModel = LaunchViewModel()

        fun bind(launch: Launch) {
            viewModel.bind(launch)
            binding.viewModel = viewModel

            itemView.setOnClickListener {
                onItemClick?.invoke(launch)
            }
        }

    }
}