package co.burcu.rocketsaidhi.ui.launch

import android.arch.lifecycle.MutableLiveData
import android.view.View
import co.burcu.rocketsaidhi.R
import co.burcu.rocketsaidhi.base.BaseViewModel
import co.burcu.rocketsaidhi.model.LaunchResponse
import co.burcu.rocketsaidhi.network.RocketApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Burcu Yalcinkaya on 05/10/2018.
 */

class LaunchListViewModel : BaseViewModel() {

    @Inject
    lateinit var rocketApi: RocketApi

    private lateinit var subscription: Disposable

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    var errorClickListener = View.OnClickListener { loadLaunches() }
    val launchListAdapter: LaunchListAdapter = LaunchListAdapter()

    init {
        loadLaunches()
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    private fun loadLaunches() {
        subscription = rocketApi.getRockets(50)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrieveLaunchListStart() }
                .doOnTerminate { onRetrieveLaunchListFinish() }
                .subscribe(
                        { result -> onRetrieveLaunchListSuccess(result) },
                        { onRetrieveRocketListError() }
                )
    }

    private fun onRetrieveLaunchListStart() {
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrieveLaunchListFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveLaunchListSuccess(list: LaunchResponse) {
        launchListAdapter.updateLaunchList(list.launches)
    }

    private fun onRetrieveRocketListError() {
        errorMessage.value = R.string.post_error
    }
}
