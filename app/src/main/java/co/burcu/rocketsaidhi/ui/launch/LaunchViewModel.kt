package co.burcu.rocketsaidhi.ui.launch

import android.arch.lifecycle.MutableLiveData
import co.burcu.rocketsaidhi.base.BaseViewModel
import co.burcu.rocketsaidhi.model.Launch

/**
 * Created by Burcu Yalcinkaya on 05/10/2018.
 */

class LaunchViewModel : BaseViewModel() {
    private var launchName = MutableLiveData<String>()
    private var launchId = MutableLiveData<Int>()
    private var launchStatus = MutableLiveData<String>()
    private var launchWindowStart = MutableLiveData<String>()
    private var launchWindowEnd = MutableLiveData<String>()

    fun bind(launch: Launch) {
        launchName.value = launch.name
        launchStatus.value = launch.status
        launchId.value = launch.id
        launchWindowStart.value = launch.windowstart
        launchWindowEnd.value = launch.windowend
    }

    fun getLaunchName(): MutableLiveData<String> {
        return launchName
    }

    fun getLaunchId(): MutableLiveData<Int> {
        return launchId
    }

    fun getLaunchStatus(): MutableLiveData<String> {
        return launchStatus
    }

    fun getLaunchWindowStart(): MutableLiveData<String> {
        return launchWindowStart
    }

    fun getLaunchWindowEnd(): MutableLiveData<String> {
        return launchWindowEnd
    }
}