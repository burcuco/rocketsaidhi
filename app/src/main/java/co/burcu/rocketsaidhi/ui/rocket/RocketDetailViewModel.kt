package co.burcu.rocketsaidhi.ui.rocket

import android.arch.lifecycle.MutableLiveData
import co.burcu.rocketsaidhi.base.BaseViewModel
import co.burcu.rocketsaidhi.model.Launch

/**
 * Created by Burcu Yalcinkaya on 06/10/2018.
 */
class RocketDetailViewModel : BaseViewModel() {
    private var rocketName = MutableLiveData<String>()
    private var launchPadId = MutableLiveData<String>()
    private var launchPadName = MutableLiveData<String>()
    private var agencyName = MutableLiveData<String>()
    private var agencyCountryCode = MutableLiveData<String>()
    private var rocketImageUrl = MutableLiveData<String>()

    fun bind(launch: Launch) {
        rocketName.value = launch.rocket.name
        launchPadName.value = launch.location.pads[0].name
        launchPadId.value = launch.location.pads[0].id.toString()
        agencyName.value = launch.location.pads[0].agencies[0].name
        agencyCountryCode.value = launch.location.pads[0].agencies[0].countryCode
        rocketImageUrl.value = launch.rocket.imageURL

    }

    fun getRocketName(): MutableLiveData<String> {
        return rocketName
    }

    fun getLaunchPadName(): MutableLiveData<String> {
        return launchPadName
    }

    fun getLaunchPadId(): MutableLiveData<String> {
        return launchPadId
    }

    fun getAgencyName(): MutableLiveData<String> {
        return agencyName
    }

    fun getAgencyCountryCode(): MutableLiveData<String> {
        return agencyCountryCode
    }

    fun getRocketImageUrl(): MutableLiveData<String> {
        return rocketImageUrl
    }

}